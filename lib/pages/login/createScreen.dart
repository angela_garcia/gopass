import 'package:flutter/material.dart';
import 'package:gopass/pages/home/home.dart';
import 'package:gopass/pages/login/inputText.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gopass/pages/services/service.dart';
import 'package:gopass/widgets/alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:core';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

enum LoginStatus { notSignIn, signIn }

class _LoginPageState extends State<LoginPage> {
  final formKey = new GlobalKey<FormState>();
  var tipoDoc = TextEditingController();
  var password = TextEditingController();
  var identificacion = TextEditingController();
  var movil = TextEditingController();
  var email = TextEditingController();
  var nombre = TextEditingController();
  var apellido = TextEditingController();

  BuildContext _ctx;
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    getPref();
    super.initState();
  }

  int isnull;

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      isnull = preferences.getInt("isnull");
    });
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    Column(
      children: [
        Container(
          width: 130,
          height: 150,
          child: Image(
            image: AssetImage('lib/assets/images/logo.png'),
          ),
        ),
      ],
    );
    var loginBtn = new Container(
        width: 300,
        height: 100,
        child: Column(
          children: [
            Container(
              width: 300,
              height: 45,
              child: RaisedButton(
                  onPressed: () {
                    print('isnull value ${isnull}');
                      _createUser();
                    if (isnull == 1) {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("!Por favor valida que se hayan guardado tus datos correctamente!"),
                            actions: <Widget>[
                              FlatButton(
                                child: Text("Volver"),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                              FlatButton(
                                child: Text("OK"),
            
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => HomePage()));
                                },
                              )
                            ],
                          );
                        },
                      );
                    } else {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Datos guardados!"),
                            actions: <Widget>[
                              FlatButton(
                                child: Text("OK"),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => HomePage()));
                                },
                              )
                            ],
                          );
                        },
                      );
                    }
                  },
                  color: Colors.greenAccent[200],
                  elevation: 0,
                  textColor: Colors.white,
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(15.0),
                  ),
                  child: Text(
                    "Registrarme",
                  )),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              width: 200,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(
                    FontAwesomeIcons.facebook,
                    size: 30,
                    color: Colors.greenAccent[200],
                  ),
                  Icon(
                    FontAwesomeIcons.twitter,
                    size: 30,
                    color: Colors.greenAccent[200],
                  ),
                  Icon(
                    FontAwesomeIcons.github,
                    size: 30,
                    color: Colors.greenAccent[200],
                  ),
                ],
              ),
            )
          ],
        ));
    var form = new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Form(
          key: formKey,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(left: 180, top: 30),
                width: 130,
                height: 90,
                child: Image(
                  image: AssetImage('lib/assets/images/logo.png'),
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  left: 40,
                  bottom: 10,
                ),
                height: 40,
                width: 400,
                child: Text("¡Bienvenido!",
                    style: TextStyle(fontSize: 25, color: Colors.blueGrey)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  width: 300,
                  height: 500,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InputText(
                          controller: tipoDoc,
                          onSaved: (val) => tipoDoc,
                          iconPath: FontAwesomeIcons.idCard,
                          placeholder: 'Tipo de documento',
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        InputText(
                          validator: (value) {
                            if (value.contains('@') && value.contains('.')) {
                              return null;
                            }
                            return "Correo no valido";
                          },
                          controller: email,
                          onSaved: (val) => email,
                          iconPath: FontAwesomeIcons.addressBook,
                          placeholder: 'Correo',
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        InputText(
                          controller: password,
                          onSaved: (val) => password,
                          iconPath: FontAwesomeIcons.lock,
                          placeholder: 'Contraseña',
                          typePassword: true,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        InputText(
                          controller: identificacion,
                          onSaved: (val) => identificacion,
                          iconPath: FontAwesomeIcons.idCardAlt,
                          placeholder: 'Identificación',
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        InputText(
                          controller: nombre,
                          onSaved: (val) => nombre,
                          iconPath: FontAwesomeIcons.user,
                          placeholder: 'Nombre',
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        InputText(
                          controller: apellido,
                          onSaved: (val) => apellido,
                          iconPath: FontAwesomeIcons.user,
                          placeholder: 'Apellidos',
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        InputText(
                          controller: movil,
                          onSaved: (val) => movil,
                          iconPath: FontAwesomeIcons.mobileAlt,
                          placeholder: 'Teléfono móvil',
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ),
        loginBtn
      ],
    );
    return new Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomPadding: false,
      body: new Container(
        child: new Center(
          child: form,
        ),
      ),
    );
  }

  Widget _createUser() {
    int service;
    return FutureBuilder(
        future: registerUser(context,
            apellidos: apellido.text,
            correo: email.text,
            nombres: nombre.text,
            numeroIdentificacion: identificacion.text,
            password: password.text,
            telefonoMovil: movil.text,
            tipoDocumento: tipoDoc.text),
        builder: (context, i) {
          print('data + ${i.hasData}');
          return Container(
              height: 50,
              width: 150,
              padding: EdgeInsets.only(left: 90),
              child: CircularProgressIndicator(
                backgroundColor: Colors.white,
                strokeWidth: 1,
              ));
        });
  }
}
