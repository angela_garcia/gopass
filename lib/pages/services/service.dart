import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

String name;
String date;
String correo;
Future<dynamic> getData(BuildContext context) async {
  final url =
      'https://apiprueba.gopass.com.co/establishment/getAllEstablishment';
  final response = await http.get(url);
  {
    Map<String, dynamic> parsed = json.decode(response.body);
    List<dynamic> data = parsed["data"];
    print(response.body);
    print(url);
    return data;
  }
}

Future<dynamic> registerUser(BuildContext context,
    {@required String correo,
    @required String tipoDocumento,
    @required String numeroIdentificacion,
    @required String nombres,
    @required String apellidos,
    @required String telefonoMovil,
    @required String password}) async {
  final url = 'https://apiprueba.gopass.com.co/client/registre';
  Map<String, dynamic> body = {
    'correo': correo,
    'tipoDocumento': tipoDocumento,
    'numeroIdentificacion': numeroIdentificacion,
    'nombres': nombres,
    'apellidos': apellidos,
    'telefonoMovil': telefonoMovil,
    'password': password
  };
  final response = await http.post(url, body: body);
  {
    final parsed = json.decode(response.body);
    final responseCode = parsed['status'] as int;
    if (responseCode == 200) {
      final prefs = await SharedPreferences.getInstance();
      prefs.setInt('isnull', 0);
    } else {
      final prefs = await SharedPreferences.getInstance();
      prefs.setInt('isnull', 1);
    }
    print(responseCode);
    print(response.body);
    print(url);
    return parsed;
  }
}
