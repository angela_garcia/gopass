import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gopass/pages/services/service.dart';
import 'package:gopass/widgets/widgetHome.dart';

BuildContext context;

class HomePage extends StatefulWidget {
  @override
  _HomePage createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[Colors.green[300], Colors.green[600]])),
          ),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              FontAwesomeIcons.arrowLeft,
              size: 17,
            ),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                FontAwesomeIcons.bars,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        ),
        body: Container(
            padding: EdgeInsets.only(left: 15, right: 10, top: 10),
            width: 350,
            child: _card()));
  }
}

Widget _card() {
  var service;
  return FutureBuilder(
      future: getData(context),
      builder: (context, i) {
        if (i.hasData) {
          final List<Widget> listCardsUser = <Widget>[];
          for (service in i.data) {
            Widget temp = WidgetHome(
              imageurl: service['imageurl'],
              direccion: service['direccion'],
              fechaCreacion: service['fechaCreacion'],
              horario: service['horario'],
              idasociado: service['idasociado'],
              idestablecimiento: service['idestablecimiento'],
              nombre: service['nombre'],
              tarifaMoto: service['tarifaMoto'],
              tarifaVehiculo: service['tarifaVehiculo'],
            );
            listCardsUser.add(temp);
            temp = null;
          }
          return Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  children: listCardsUser,
                ),
              ),
            ],
          );
        }
        return Container(
            height: 50,
            width: 150,
            padding: EdgeInsets.only(left: 90),
            child: CircularProgressIndicator(
              backgroundColor: Colors.white,
              strokeWidth: 1,
            ));
      });
}
