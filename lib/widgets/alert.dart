import 'package:flutter/material.dart';
import 'package:gopass/pages/home/home.dart';

class Alert extends StatefulWidget {
  final String apellidos;
  final String correo;
  final String nombres;
  final String numeroIdentificacion;
  final String password;
  final String telefonoMovil;
  final String tipoDocumento;

  const Alert(
      {Key key,
      this.apellidos,
      this.correo,
      this.nombres,
      this.numeroIdentificacion,
      this.password,
      this.telefonoMovil,
      this.tipoDocumento})
      : super(key: key);

  @override
  _AlertHome createState() => _AlertHome();
}

class _AlertHome extends State<Alert> {
  @override
  Widget build(BuildContext context) {
    showDialog(
              context: context,
              barrierDismissible: false, // user must tap button!
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('¡Datos guardados!'),
                  content: SingleChildScrollView(
                    child: ListBody(
                      children: <Widget>[
                        
                        Text('Nombre: ${this.widget.nombres}'),
                        Text('Correo: ${this.widget.correo}'),
                        Text('Telefono: ${this.widget.telefonoMovil}')
                        
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    RaisedButton(
                      child: Text('Ok'),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomePage()));
                        // Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
  }
}
