import 'package:flutter/material.dart';

class WidgetHome extends StatefulWidget {
  final String idestablecimiento;
  final String idasociado;
  final String nombre;
  final String razonsocial;
  final String direccion;
  final String tarifaVehiculo;
  final String tarifaMoto;
  final String fechaCreacion;
  final String imageurl;
  final String horario;

  const WidgetHome(
      {Key key,
      this.idasociado,
      this.idestablecimiento,
      this.nombre,
      this.razonsocial,
      this.direccion,
      this.tarifaVehiculo,
      this.tarifaMoto,
      this.horario,
      this.fechaCreacion,
      this.imageurl})
      : super(key: key);

  @override
  _WidgetHomeState createState() => _WidgetHomeState();
}

class _WidgetHomeState extends State<WidgetHome> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160,
      width: 300,
      child: Container(
        width: 10,
        height: 10,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          color: Colors.green[100], //here
          elevation: 1,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 10),
                child: ListTile(
                  leading: Image.network(
                          'https://miro.medium.com/max/700/0*H3jZONKqRuAAeHnG.jpg') ??
                      'https://miro.medium.com/max/700/0*H3jZONKqRuAAeHnG.jpg',
                  title: Text(this.widget.nombre,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14)),
                  subtitle: Text(
                      "Tarifa Vehiculo: ${this.widget.tarifaVehiculo} \n"
                      "Tarifa moto: ${this.widget.tarifaMoto}\n "
                      "Fecha Creacion: ${this.widget.fechaCreacion}",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                          fontWeight: FontWeight.bold)),
                ),
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 20),
                    width: 300,
                    height: 30,
                    child: Text(
                     'Direccion: ${ this.widget.direccion} \n',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                  
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
